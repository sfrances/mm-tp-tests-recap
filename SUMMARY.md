# Summary

* [Home](README.md)
* [General workflow](workflow.md)
* [Turning on things](turnon.md)
* [Felix servers](felix.md)
* [Partitions](partition.md)
* [SBC and TTC control](sbc.md)
* [Miscellanea](misc.md)
* [Logbook](logbook.md)
  * [Week 27](week2021_27.md)
  * [Week 28](week2021_28.md)
  * [Week 29](week2021_29.md)
