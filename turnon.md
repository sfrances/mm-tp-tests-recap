# VS TPs
There are currently two TPs in the VS setup, one for sTGC and one for MM.
Both are turned on by the same switch here  
http://nsw-pdu01.cern.ch  
(reachable inside CERN campus)

Turn on the ATCA crate if it's not on. The crate should turn on and after 10-30 seconds, the green lights on the mezzanine card should turn on. Also check for blue lights (related to issue RK and SF found in 191?). The fans will lower their speed and then come back up. At this point, the TPs should be ready for programming.

It is not possible to switch on/off MM and sTGC TPs separately. It is thus possible to cool down the MM TP once the the work is done with  
`ipmitool -H 192.168.0.171 -P "" -t 0x82 mc reset cold #only doable from pcatlnswfelix01`  
So to remove the heavy firmware and avoid crate boiling (especially if someone is running in parallel on sTGC side).
