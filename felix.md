# VS sTGC - Felix01
* Connect to felix01. This is accessible only inside CERN local network.  
`ssh -XY nswdaq@pcatlnswfelix01`
* Configure the elinks.  
`elinkconfig_MMTP_VS`  
This is an alias, check it for more info.
* Open a screen session. Typically at the VS people are leaving screen sessions detached. Check with `-ls` if there are running sessions before opening a new one.  
`screen (-r)`  
In case you are running a new session, `source .bash_profile` for getting all the aliases and the common stuff.
* In tab 4 (progFPGA) configure the TP  
`progTP`  
and check the links
`flx-info GBT`
(investigate with RK the expected link configuration)
* In tab 0 (flx) open felixcore  
`felixcore --data-interface=eno1 --elinks=673,677` (with default felix-id)
* In tab 1 (opc)  
`OpcUaScaServer /afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/opc_xml/VS/vs_MM_1tpscax.xml` (understand better this config file and its meaning/content)
* In tab 2 (netio)  
`netio_cat subscribe -H pcatlnswfelix01 -p 12350 -e raw -t 673` (understand with RK why 12350 and 673 and their relation with felix-id)  
and possibly redirect the output to a txt file.

# VS MM - Felix10
* Connect to felix10. This is accessible only inside CERN local network.  
`ssh -XY nswdaq@pcatlnswfelix10`
* Configure the elinks.  
`elinkconfig_VS_MMTP_SCA_full_setup`  
This is an alias, check it for more info.
* Open a screen session. Typically at the VS people are leaving screen sessions detached. Check with `-ls` if there are running sessions before opening a new one.  
`screen (-r)`  
In case you are running a new session, `source .bash_profile` for getting all the aliases and the common stuff.
* In tab 4 (progFPGA) configure the TP  
`progTP`  
and check the links
`flx-info GBT`
(investigate with RK the expected link configuration)
* In tab 0 (flx) open felixcore  
`felixcore --data-interface=eno1 --elinks=649,673,677,827 --felix-id 10`
* In tab 1 (opc)  
`OpcUaScaServer /afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/opc_xml/VS/MMTrigger/vs_MMTP_felix10.xml` (understand better this config file and its meaning/content)
* In tab 2 (netio)
```
netio_cat subscribe -H pcatlnswfelix10 -p 12350 -e raw -t 656033 #this should be the relevant one
netio_cat subscribe -H pcatlnswfelix10 -p 12350 -e raw -t 656037
netio_cat subscribe -H pcatlnswfelix10 -p 12350 -e raw -t 656009
```
(understand with RK why 12350 and 656033, 656037, 656009 and their relation with felix-id and which poit is the felix output to swROD) and possibly redirect the output to a txt file.


# Critical config files
## OPC xml files
These files contain lists of SCA blocks. You should select only the ones needed for your work. They are (or should be) all in `/afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/opc_xml/`.
* `VS/vs_MM_1tpscax.xml`  
Configure to work on VS sTGS side; not sure about the exact content
* `VS/MMTrigger/vs_MM_8mmfe8_R2R14_1tpscax_felix10.xml`  
Full chain setup; made by XJ (ask her for more info)
* `VS/MMTrigger/vs_MMTP_felix10.xml`  
Only TP on VS MM side; made by SF (still in testing); it make use of all common xml packages (as the XJ one)
