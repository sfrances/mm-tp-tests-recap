# SBC to control TTC  
* Connect to SBC NSW server. This is accessible only inside CERN local network.  
`ssh -XY nswdaq@sbcnsw-ttc-01`  
* Setup the environment  
`tdaq tdaq-09-02-01` or `setupSBC` (what is the proper one?)
* Initialize ALTI (needed?)  
`altiInit`

Once the setup is done you can start sending patterns with
`oneshot 13 && sleep 5 && start_pattern 13 && sleep <number of seconds here> && stop_pattern 13`  
