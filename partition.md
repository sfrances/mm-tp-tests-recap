# Running the partition
* Connect to swROD. This is accessible only inside CERN local network.  
`ssh -XY  nswdaq@pcatlnswswrodvs`
* Setup the environment with  
```
source ~/workspace/public/nswdaq/current/nswdaq/setup_nswdaq.sh
source ~/workspace/public/nswdaq/current/nswdaq/installed/setup_pm.sh
```
or use the `partmaker` alias which is also nicely heading to  
`/afs/cern.ch/user/n/nswdaq/workspace/public/nswdaq/current/databases/`  
(where all partitions `.yml` files are stored)
* Edit `.yml` file in case of need. The file  
`/afs/cern.ch/user/n/nswdaq/workspace/public/nswdaq/current/databases/VS-MMTP-Rate-Simone.yml`  
is a starting point. It setup only the minimum required for MM TP readout tests with different pattern files.
* Create the partition.  
`makeNSWPartition.py VS-MMTP-Rate-Simone.yml`  
It is possible to just upgrade (`-U`) the previous one (faster?) but it's nice to always run with a fresh one. Also, the `--nosan` flag can be called in case of issue with the `Sanitizer` during the run.
* Setup the partition  
`source /afs/cern.ch/user/n/nswdaq/workspace/public/nswdaq/current/databases/VS-MMTP-Rate-Simone/setup_part.sh`
* To run the partition `run`  
To kill the partition `killit` (or graphical interface but in case be sure to click on __Close and exit partition__)
* Tips after running the partition:
  * Check in the __Access Control__ the __Control__ flag
  * Mark __IgniPanels.DFPanel.DFPanel__ in __Load Panels__
  * Fully expand NSW tree

# Critical config files
## Partion yml files
This is the first ingredient to be checked.
* Check that the proper pattern file is called
* Check working directory and swROD output folder (standard choice `/tmp/nswdaq/`)
* Check the ALTI slot (e.g. 13 for Felix10, 9 for Felix01)
* Check the elinks to MM TP (e.g. if on Felix10 and you called felix-core with --felix-id 10, this should be `0xa02a1` for the L1A data path)
* Check the db config `json` file (`config.db`). It should be consistent with the setup (especially Felix servers names, etc.). E.g. `VS-MMTP-Rate-Simone.yml` has `/afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/config_json/VS/art_phase.1595496565.2020_07_24_10h32m28s_OnlyTP.json` which should be configure with just common stuff and for MM TP (connected to `pcatlnswfelix10.cern.ch:48020`)

## Db config json files
See last point of previous section. Comment more once you understood more.
